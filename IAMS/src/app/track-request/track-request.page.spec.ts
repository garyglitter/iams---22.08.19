import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackRequestPage } from './track-request.page';

describe('TrackRequestPage', () => {
  let component: TrackRequestPage;
  let fixture: ComponentFixture<TrackRequestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackRequestPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackRequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
