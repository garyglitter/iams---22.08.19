import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'toggle-content-approved',
  templateUrl: './toggle-content-approved.component.html',
  styleUrls: ['./toggle-content-approved.component.scss'],
})
export class ToggleContentApprovedComponent implements OnInit {

  @Input() show : Boolean;

  visible: boolean = false;

 
  constructor(private route: ActivatedRoute,
    private requestService: RequestServiceService) { }

  ngOnInit() {
   
  }


  toggle() {
    this.visible = !this.visible;
  }
}
