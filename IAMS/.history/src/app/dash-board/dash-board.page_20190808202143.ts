import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.page.html',
  styleUrls: ['./dash-board.page.scss'],
})
export class DashBoardPage implements OnInit, OnDestroy, AfterViewInit {

  

  constructor(private statusBar: StatusBar,private platform: Platform) {
    this.statusBar.overlaysWebView(true);
    this.statusBar.backgroundColorByHexString('#1761A0');
    
    this.platform.registerBackButtonAction(() => {
      const overlay = this.app._appRoot._overlayPortal.getActive();
      const nav = this.app.getActiveNav();
      const closeDelay = 2000;
      const spamDelay = 500;
    
      if(overlay && overlay.dismiss) {
        overlay.dismiss();
      } else if(nav.canGoBack()){
        nav.pop();
      } else if(Date.now() - this.lastBack > spamDelay && !this.allowClose) {
        this.allowClose = true;
        let toast = this.toastCtrl.create({
          message: this.translate.instant("general.close_toast"),
          duration: closeDelay,
          dismissOnPageChange: true
        });
        toast.onDidDismiss(() => {
          this.allowClose = false;
        });
        toast.present();
      } else if(Date.now() - this.lastBack < closeDelay && this.allowClose) {
        this.platform.exitApp();
      }
      this.lastBack = Date.now();
    });
   }

  ngOnInit() {
 
 
  }
  ngAfterViewInit() { 

  }
  ngOnDestroy() {
    
   }



  tap(){
    console.log("printf")
  }


}
