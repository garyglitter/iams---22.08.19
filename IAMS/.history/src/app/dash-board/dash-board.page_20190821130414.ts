import { Component, OnInit, OnDestroy, AfterViewInit } from "@angular/core";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Platform } from "@ionic/angular";
import { AlertController } from "@ionic/angular";
import { RequestServiceService } from '../my-pending-approval/request-service.service';

@Component({
  selector: "app-dash-board",
  templateUrl: "./dash-board.page.html",
  styleUrls: ["./dash-board.page.scss"]
})
export class DashBoardPage implements OnInit, OnDestroy, AfterViewInit {
  subscription;
  constructor(
    private statusBar: StatusBar,
    public alertController: AlertController,
    private platform: Platform,
    private requestService :RequestServiceService
    
  ) {
    this.statusBar.overlaysWebView(true);
    this.statusBar.backgroundColorByHexString("#1761A0");

  
  }

  ngOnInit() {
    this.requestService.dummyData
  }
  ngAfterViewInit() {
    this.subscription=this.platform.backButton.subscribe(()=>{
      navigator['app'].exitApp();
    })
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  tap() {
    console.log("printf");
  }
}
