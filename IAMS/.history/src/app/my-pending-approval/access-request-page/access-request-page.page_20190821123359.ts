import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { RequestServiceService } from "../request-service.service";
import { PendingRequest } from "../pending-request";
import { AlertController } from "@ionic/angular";
import { PendingRequestDetailed } from "../pending-request-detailed";
import { ToastController } from '@ionic/angular';
@Component({
  selector: "app-access-request-page",
  templateUrl: "./access-request-page.page.html",
  styleUrls: ["./access-request-page.page.scss"]
})
export class AccessRequestPagePage implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private requestService: RequestServiceService,
    public alertController: AlertController,
    public toastController: ToastController
  ) {}
  accessName;
  transactionNumber: string;
  business: string;
  role: string;
  desiredDate: string;
  purpose: string;
  description : string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam"
  pendingRequestDetailed : PendingRequestDetailed[] =[];
  ngOnInit() {
    this.route.params.subscribe(params => {
      let id = +params["id"];
      let business = params["business"];
      let transactionNumber = params["transactionNumber"];
      let purpose = params["purpose"];
      let desiredDate = params["desiredDate"];
      let role = params["role"];
      console.log(id)
      this.transactionNumber = transactionNumber;
      this.business = business;
      this.purpose = purpose;
      this.desiredDate = desiredDate;
      this.role = role;

      // this.requestService.dummyData.subscribe(data=>{
      //   data.filter((value)=>{
      //     if(value.id === id){
      //       console.log(value.PendingRequestDetailed)
      //       this.pendingRequestDetailed = value.PendingRequestDetailed;
      //     }
      //   })
      // })
      
    });
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      message: "Are you sure want to Reject?",
      buttons: ["CANCEL", "YES"]
    });

    await alert.present();
  }
close(){
  console.log("test fun")
}
  async presentToast() {
    console.log("inside toasr")
    const toast = await this.toastController.create({
      message: 'Approved Successfully',
      duration: 2000,
      position: "top",
      buttons:[
       {
         icon:"star"
       }
      ]
    });
    toast.present();
  }
}
