import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MyPendingApprovalPage } from './my-pending-approval.page';
import { ToggleContentComponent } from './toggle-content/toggle-content.component';

const routes: Routes = [
  {
    path: '',
    component: MyPendingApprovalPage
  },
  
    { path: 'access-request-page', loadChildren: './my-pending-approval/access-request-page/access-request-page.module#AccessRequestPagePageModule' }
  
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MyPendingApprovalPage,ToggleContentComponent]
})
export class MyPendingApprovalPageModule {}
