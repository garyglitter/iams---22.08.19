export class PendingRequestDetailed {
    id: number | string;
    transactionNumber : string;
    business : string;
    role :string;
    desiredDate : string;
    purpose :string;
}
