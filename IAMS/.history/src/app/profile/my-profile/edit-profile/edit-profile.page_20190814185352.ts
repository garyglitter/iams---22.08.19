import { Component, OnInit } from '@angular/core';
import { PhotoServiceService } from 'src/app/commonService/photo-service.service';
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

  constructor(public photoService :PhotoServiceService) { }

  fakeImage: boolean = false;
  ngOnInit() {
    this.photoService.loadSaved();
    if (this.photoService.photos.length < 0) {
      this.fakeImage = true;
    }
  }

  clear()
}
