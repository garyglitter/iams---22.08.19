import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PhotoServiceService } from 'src/app/commonService/photo-service.service';
@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class MyProfilePage implements OnInit {

  constructor(private camera: Camera,public photoService : PhotoServiceService) { }

  ngOnInit() {
  }
  // image:any=''
  // openCam(){

  //   const options: CameraOptions = {
  //     quality: 100,
  //     destinationType: this.camera.DestinationType.FILE_URI,
  //     encodingType: this.camera.EncodingType.JPEG,
  //     mediaType: this.camera.MediaType.PICTURE
  //   }
    
  //   this.camera.getPicture(options).then((imageData) => {
  //    // imageData is either a base64 encoded string or a file URI
  //    // If it's base64 (DATA_URL):
  //    //alert(imageData)
  //    this.image=(<any>window).Ionic.WebView.convertFileSrc(imageData);
  //   }, (err) => {
  //    // Handle error
  //    alert("error "+JSON.stringify(err))
  //   });

  // }
}
