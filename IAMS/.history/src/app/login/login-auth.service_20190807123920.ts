import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class LoginAuthService implements CanActivate {

  constructor(private router: Router) { }

   dummy_response = {
    username: "gary@g",
    password: "gary"
  };
  this.router.events.subscribe((res) => { 
    console.log(this.router.url,"Current URL");
})
  canActivate(route: ActivatedRouteSnapshot): boolean {


    console.log(route);

    let authInfo = {
        authenticated: true
    };

    if (!authInfo.authenticated) {
        this.router.navigate(['login']);
        return false;
    }

    return true;

}
}
